from typing import Set, Tuple

def process_engine_map(engine_map):
    part_numbers = set()
    asterisk_coords = set()
    sum = 0

    for i, row in enumerate(engine_map):
        for j, symbol in enumerate(row):
            if symbol.isdigit():
                part_number = get_part_number(engine_map, j, i)
                if part_number:
                    part_numbers.add(part_number)

            elif symbol == '*':
                asterisk_coords.add((j, i))

    for x, y in asterisk_coords:
        part_numbers_at_asterisk = [part_numbers.pop() for _ in range(2) if (x, y) in asterisk_coords and part_numbers]
        if len(part_numbers_at_asterisk) == 2:
            sum += part_numbers_at_asterisk[0] * part_numbers_at_asterisk[1]

    print(sum)

def get_part_number(engine_map, x, y):
    part_number = ''
    x_start, y_start = x - 1, y - 1
    for i in range(x_start, x_start + 3):
        for j in range(y_start, y_start + 3):
            try:
                if i >= 0 and j >= 0 and engine_map[j][i].isdigit():
                    part_number += engine_map[j][i]
            except IndexError:
                continue

    return int(part_number) if part_number else None

def main():
    with open('../input/day3.txt', 'r') as file:
        engine_map = [line.strip() for line in file]

    process_engine_map(engine_map)

if __name__ == "__main__":
    main()
