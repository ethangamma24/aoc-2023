from aoc_utils import *

scratch_copies = {}

def main():
    # print(handle_line_int('../input/day4.txt', calculate_scratch_points))
    print(handle_line_int('../input/day4.txt', calculate_scratch_copies))

def calculate_scratch_points(line):
    sum = 0
    line = line.strip('\n')
    winning_nums_string = get_contents_between_chars(line, ': ', ' |')
    print(line)
    my_nums_string = line.split('| ')[1]

    winning_nums = winning_nums_string.split(' ')
    my_nums = my_nums_string.split(' ')

    for num in my_nums:
        if num in winning_nums and num != '':
            print(num)
            if sum == 0:
                sum = 1
            else:
                sum *= 2

    return sum

def calculate_scratch_copies(line):
    sum = 0
    line = line.strip('\n')

    winning_nums_string = get_contents_between_chars(line, ': ', ' |')
    print(line)
    my_nums_string = line.split('| ')[1]

    winning_nums = winning_nums_string.split(' ')
    my_nums = my_nums_string.split(' ')

    initialize_scratch_cards_dictionary()

    for num in my_nums:
        if num in winning_nums and num != '':
            sum += 1
    
    for i in range(sum):
        # TODO: Get current number of card (maybe I should take in the whole input and iterate over the range
        #       of its len?) and add the number of cards won.

# Initialize the scratch cards dict with one card each initially
def initialize_scratch_cards_dictionary():
    with open('../input/day4.txt') as f:
        input = f.readlines()
        for i in range(len(input)):
            scratch_copies[i] = 1
    
    print(scratch_copies)

if __name__ == "__main__":
    main()