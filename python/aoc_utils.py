from typing import List

def handle_line_int(file_path, function) -> int:
    _sum = 0
    with open(file_path) as f:
        _input = f.readlines()
        for _line in _input:
            _sum += function(_line)
    return _sum

def handle_file_int(file_path, function) -> int:
    with open(file_path) as f:
        _input = f.readlines()
        return function(_input)

def read_double_array(input) -> List[List[str]]:
    double_array = []
    for line in input:
        # Split each line into individual characters, getting rid of any
        # newline characters.
        characters = list(line.strip())
            
        # Add the list of characters to the 2D array
        double_array.append(characters)

    return double_array

def get_contents_between_chars(input_string, char_start, char_end):
    start_index = input_string.find(char_start)
    end_index = input_string.find(char_end, start_index + 1)

    if start_index != -1 and end_index != -1:
        return input_string[start_index + 1:end_index]
    else:
        return None