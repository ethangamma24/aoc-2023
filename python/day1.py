import json
import os
import re

english_to_int = {
    'zero': '0',
    'one': '1',
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9'
}

def main():
    # part_one()
    part_two()

def part_one():
    sum = 0
    with open('../input/day1.txt') as f:
        input = f.readlines()
        for line in input:
            sum += handle_line(line)
    print(sum)

def part_two():
    sum = 0
    with open('../input/day1.txt') as f:
        input = f.readlines()
        for line in input:
            #sum += handle_line(convert_english_to_ints(line))
            sum += handle_line_with_words(line)
    print(sum)
                    
def handle_line(line) -> int:
    first_num = ''
    last_num = ''
    for char in line:
        if char.isdigit():
            if first_num == '':
                first_num = str(char)
            else:
                last_num = str(char)
    if last_num == '':
        last_num = first_num
    sum = int(first_num + last_num)
    return sum

def handle_line_with_words(line) -> int:
    number_index_map = {}
    result = ''

    for word, number in english_to_int.items():
        index = line.find(word)
        while index != -1:
            number_index_map[index] = number
            index = line.find(word, index + 1)

    for i in range(len(line)):
        if line[i].isdigit():
            number_index_map[i] = line[i]

    for key, value in dict(sorted(number_index_map.items())).items():
        result += str(value)

    sum = int(result[0] + result[len(result) - 1])
    return sum

'''
def convert_english_to_ints(line) -> str:
    result = ''
    i = 0

    # Regex pattern that matches English spelling of integers
    pattern = re.compile('|'.join(re.escape(word) for word in english_to_int.keys()) + r'|\d+', re.IGNORECASE)

    matches = pattern.findall(line)

    # result = ''.join(english_to_int[match.lower()] if match.lower() in english_to_int else match for match in matches)
    
    while i < len(matches):
        curr_match = matches[i].lower()

        if curr_match in english_to_int:
            result += english_to_int[curr_match]
            i += 1
        else:
            # Check for word combos
            combination = curr_match + matches[i + 1].lower() if i + 1 < len(matches) else ''
            if combination in english_to_int:
                result += english_to_int[combination]
                i += 2
            else:
                result += matches[i]
                i += 1

    print(line)
    print(result)
    return result
'''

if __name__ == "__main__":
    main()
