import json
from typing import Dict, List
from aoc_utils import handle_file_int, read_double_array

def main():
    # print(handle_file_int('../input/day3.txt', add_part_numbers))
    print(handle_file_int('../input/day3.txt', multiply_gear_numbers))

def add_part_numbers(input):
    sum = 0
    x, y = 0, 0
    # Starting this as a string so I can concat them together, then convert
    # to int
    curr_number = ''
    looking_at_part = False
    engine_map = input

    '''
    TODO: create a bool to track if we are getting part number. Track index of
          beginning of part number. Once part number is got, search around it
          for symbol.
    '''
    print(engine_map)
    for i in range(len(engine_map)):
        for j in range(len(engine_map[i])):
            if engine_map[i][j].isdigit():
                curr_number += str(engine_map[i][j])
                # Grabbing the coordinates of the first digit of the potential part number.
                if looking_at_part is False:
                    x, y = j, i
                    print(f"Set x, y: {x}, {y}")
                    looking_at_part = True
            elif curr_number != '':
                looking_at_part = False
                print(curr_number)
                sum += determine_part_number(curr_number, engine_map, x, y)
                curr_number = ''

    print(sum)

# Passing in the potential part number we're looking at, along with the x, y
# coordinates of the last digit in the number so we know where to search in
# the engine map.
def determine_part_number(number, engine_map, x, y) -> int:
    # for digit in range(len(number)):
    length, x_start, y_start = len(number), x - 1, y - 1
    # Start at the x coordinate before the first digit of the potential part number, then
    # end at the x coordinate after the last digit.
    for i in range(x_start, x_start + length + 2):
        for j in range(y_start, y_start + 3):
            try:
                if (i > -1 and j > -1):
                    if engine_map[j][i] not in ('.', '\n', '') and not engine_map[j][i].isdigit():
                        print(f"Found symbol: {engine_map[j][i]}")
                        return int(number)
            except:
                continue
    return 0

def multiply_gear_numbers(input):
    sum = 0
    x, y = 0, 0
    # Starting this as a string so I can concat them together, then convert
    # to int
    curr_number = ''
    looking_at_part = False
    asterisk_map = {}
    engine_map = input

    for i in range(len(engine_map)):
        for j in range(len(engine_map[i])):
            if engine_map[i][j].isdigit():
                curr_number += str(engine_map[i][j])
                # Grabbing the coordinates of the first digit of the potential part number.
                if looking_at_part is False:
                    x, y = j, i
                    print(f"Set x, y: {x}, {y}")
                    looking_at_part = True
            elif curr_number != '':
                looking_at_part = False
                print(curr_number)
                asterisk_coords = find_asterisk(curr_number, engine_map, x, y)
                tupled_coords = tuple(asterisk_coords)
                if asterisk_coords != [-1, -1] and tupled_coords not in asterisk_map:
                    asterisk_map[tupled_coords] = [int(curr_number)]
                elif asterisk_coords != [-1, -1]:
                    asterisk_map[tupled_coords].append(int(curr_number))
                curr_number = ''
    print(asterisk_map)
    for coords, numbers in asterisk_map.items():
        print(numbers)
        if len(numbers) == 2:
            sum += (numbers[0] * numbers[1])

    print(sum)

def find_asterisk(number, engine_map, x, y) -> Dict[int, int]:
    length, x_start, y_start = len(number), x - 1, y - 1
    for i in range(x_start, x_start + length + 2):
        for j in range(y_start, y_start + 3):
            try:
                if (i > -1 and j > -1):
                    # print(f"(x,y): ({i},{j}): {engine_map[j][i]}")
                    if engine_map[j][i] in ('*'):
                        print(f"Found symbol: {engine_map[j][i]}")
                        return [j,i]
            except:
                # print(f"Probably invalid array address: (x,y): ({i},{j})")
                continue
    return [-1,-1]

if __name__ == "__main__":
    main()
