import json

BAG_CONTENTS = {
    'red': 12,
    'green': 13,
    'blue': 14
}

def main():
    # part_one()
    part_two()

def part_one():
    sum = 0
    with open('../input/day2.txt') as file:
        lines = file.readlines()
        for line in lines:
           sum += possible_games(line)
    print(sum)

def part_two():
    sum = 0
    with open('../input/day2.txt') as file:
        lines = file.readlines()
        for line in lines:
           sum += minimum_cubes(line)
    print(sum)

def possible_games(line) -> int:
    line = line.strip('\n')
    game_id = line.split(':')[0][5:]
    # Grabs the 'hands' from the game and puts them in a list.
    hands = line.split(':')[1][1:].split('; ')
    for hand in hands:
        cubes = hand.split(', ')
        for cube in cubes:
            num_cubes = cube.split(' ')[0]
            color = cube.split(' ')[1]
            if int(num_cubes) > BAG_CONTENTS[color]:
                return 0
            
    return int(game_id)

def minimum_cubes(line) -> int:
    line = line.strip('\n')
    cube_count = {
        'red': 0,
        'blue': 0,
        'green': 0
    }
    hands = line.split(':')[1][1:].split('; ')
    for hand in hands:
        cubes = hand.split(', ')
        for cube in cubes:
            num_cubes = int(cube.split(' ')[0])
            color = cube.split(' ')[1]
            if num_cubes > cube_count[color] or cube_count[color] == 0:
                cube_count[color] = num_cubes

    power = cube_count['red'] * cube_count['blue'] * cube_count['green']
            
    return power


if __name__ == "__main__":
    main()
